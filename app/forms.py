__author__ = 'Natallia'

from flask.ext.wtf import Form
from wtforms import BooleanField, PasswordField, StringField, SubmitField
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired
from .models import User


class LoginForm(Form):
    email = EmailField('email', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    remember_me = BooleanField('remember_me', default=False)


class RegisterForm(Form):
    nickname = StringField('nickname', validators=[DataRequired("Please, enter a nickname")])
    email = EmailField('email', validators=[DataRequired("Please, enter email")])
    password = PasswordField('password', validators=[DataRequired("Please, enter password")])
    submit = SubmitField("Create account")

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)

    def validate(self):
        if not Form.validate(self):
            return False
        user1 = User.query.filter_by(email=self.email.data.lower()).first()
        user2 = User.query.filter_by(nickname=self.nickname.data.lower()).first()
        if user1:
            self.email.errors.append("This email is already taken")
        if user2:
            self.nickname.errors.append("This nickname is already taken")
        if user1 or user2:
            return False
        else:
            return True
