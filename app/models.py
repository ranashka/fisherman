__author__ = 'Natallia'

from app import db, bcrypt


class User(db.Model):
    """An admin user capable of viewing reports.

    :param str email: email address of user
    :param str password: encrypted password for the user

    """
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    nickname = db.Column(db.String(10), index=True, unique=True)
    email = db.Column(db.String(30), index=True, unique=True)
    passwordHash = db.Column(db.String)
    social_id = db.Column(db.String(64), nullable=True, unique=True)

    def __init__(self, nickname, email, password='', social_id=None):
        self.social_id = social_id
        self.nickname = nickname.title()
        if email:
            self.email = email.lower()
        if self.social_id is None:
            self.set_password(password)

    def set_password(self, password):
        self.passwordHash = bcrypt.generate_password_hash(password)

    def check_password(self, password):
        return bcrypt.check_password_hash(self.passwordHash, password)

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        try:
            return unicode(self.id)  # python 2
        except NameError:
            return str(self.id)  # python 3

class Zec(db.Model):
    __tablename__ = "zec"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40), unique=True, nullable=False)
    opening_hours = db.Column(db.String(100))
    lang = db.Column(db.String(20))
    latitude = db.Column(db.FLOAT)
    longitude = db.Column(db.FLOAT)
    address = db.Column(db.String(100))
    description = db.Column(db.String(500))
    rules = db.Column(db.String(500))
    lakes = db.relationship('Lake', backref='zec')


    def __init__(self, name=None, opening_hours='', lang='', latitude='', longitude='', address='',
                 description='', rules=''):
        self.name = name
        self.opening_hours = opening_hours
        self.lang = lang
        self.latitude = latitude
        self.longitude = longitude
        self.address = address
        self.description = description
        self.rules = rules
        self.lakes = self.lakes

    def __repr__(self):
        return '%r' % self.name

    def get_id(self):
        try:
            return unicode(self.id)  # python 2
        except NameError:
            return str(self.id)  # python 3


connects = db.Table('connects',
                    db.Column('fish_id', db.Integer, db.ForeignKey('fish.id')),
                    db.Column('lake_id', db.Integer, db.ForeignKey('lake.id')))


class Lake(db.Model):
    __tablename__ = "lake"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40), index=True, unique=True, nullable=False)
    latitude = db.Column(db.FLOAT)
    longitude = db.Column(db.FLOAT)
    square = db.Column(db.FLOAT)
    max_depth = db.Column(db.FLOAT)
    description = db.Column(db.String)
    fish = db.relationship('Fish', secondary=connects, backref=db.backref('lakes', ))
    boat_downhill = db.Column(db.BOOLEAN)
    zecID = db.Column(db.Integer, db.ForeignKey('zec.id'))
    photos = db.relationship('Photo', backref='lake')
    videos = db.relationship('Video', backref='lake')


    def __init__(self, name=None, latitude='', longitude='', square='', max_depth='', description='',
                 fish=[], boat_downhill='', zecID=''):
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
        self.square = square
        self.max_depth = max_depth
        self.description = description
        self.fish = fish
        self.boat_downhill = boat_downhill
        self.zecID = zecID

    def __repr__(self):
        return '%r' % self.name

    def get_id(self):
        try:
            return unicode(self.id)  # python 2
        except NameError:
            return str(self.id)  # python 3


class Fish(db.Model):
    __tablename__ = "fish"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40), index=True, unique=True, nullable=False)
    description = db.Column(db.String(500))
    how_to_catch = db.Column(db.String(500))

    def __init__(self, name='', description='', how_to_catch=''):
        self.name = name
        self.description = description
        self.how_to_catch = how_to_catch

    def __repr__(self):
        return '%r' % self.name

    def get_id(self):
        try:
            return unicode(self.id)  # python 2
        except NameError:
            return str(self.id)  # python 3


class Photo(db.Model):
    __tablename__ = "photo"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40), unique=True, nullable=False)
    link = db.Column(db.String, unique=True, nullable=False)
    description = db.Column(db.String(500))
    place = db.Column(db.String(20))
    date = db.Column(db.Date)
    lakeID = db.Column(db.Integer,  db.ForeignKey('lake.id'))

    def __init__(self, name=None, link=None, description='', place='', date='', zecID='', lakeID=''):
        self.name = name
        self.link = link
        self.description = description
        self.place = place
        self.date = date
        self.zecID = zecID
        self.lakeID = lakeID

    def __repr__(self):
        return '%r' % self.link

    def get_id(self):
        try:
            return unicode(self.id)  # python 2
        except NameError:
            return str(self.id)  # python 3


class Video(db.Model):
    __tablename__ = "video"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40), unique=True, nullable=False)
    link = db.Column(db.String, unique=True)
    description = db.Column(db.String(500))
    place = db.Column(db.String(20))
    date = db.Column(db.Date)
    lakeID = db.Column(db.Integer,  db.ForeignKey('lake.id'))

    def __init__(self, name=None, link=None, description='', place='', date='', zecID='', lakeID=''):
        self.name = name
        self.link = link
        self.description = description
        self.place = place
        self.date = date
        self.zecID = zecID
        self.lakeID = lakeID

    def __repr__(self):
        return '%r' % self.link


    def get_id(self):
        try:
            return unicode(self.id)  # python 2
        except NameError:
            return str(self.id)  # python 3