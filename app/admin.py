__author__ = 'Natallia'

import os

from flask_admin import Admin, form
from flask_admin.contrib.sqla import ModelView
from sqlalchemy.event import listens_for
from jinja2 import Markup

from app import db
from .models import User, Zec, Lake, Fish, Photo, Video


# Create directory for file fields to use
photo_path = os.path.join(os.path.dirname(__file__), 'static/photos')
video_path = os.path.join(os.path.dirname(__file__), 'static/videos')
try:
    os.mkdir(photo_path)
    os.mkdir(video_path)
except OSError:
    pass

admin = Admin(name='Admin')

class UserView(ModelView):
    column_exclude_list = ['passwordHash', 'social_id']
    column_searchable_list = ['nickname', 'email']
    can_create = False
    can_edit = False
    can_delete = False
    column_default_sort = 'nickname'


def format_lake(view, context, model, name):
    if not model.lakes:
        return []
    else:
        return model.lakes

def format_img(view, context, model, name):
    if not model.link:
        return ''
    else:
        return Markup('<img src="%s">' % ('/static/photos/' + form.thumbgen_filename(model.link)))

class ZecView(ModelView):
    form_columns = ('name', 'opening_hours', 'lang', 'latitude', 'longitude', 'address', 'description', 'rules',
                    'lakes')
    column_list = ('name', 'opening_hours', 'lang', 'latitude', 'longitude', 'address', 'description', 'rules', 'lakes')
    column_searchable_list = ['name', ]
    column_default_sort = 'name'
    form_choices = {'lang': [('en', 'EN'), ('fr', 'FR'), ('en/fr', 'EN/FR')]}
    column_formatters = {'lakes': format_lake}
    column_editable_list = ['name', 'opening_hours', 'lang', 'latitude', 'longitude', 'address', 'description', 'rules']
    create_modal = True
    edit_modal = True


class LakeView(ModelView):
    form_columns = ('name', 'latitude', 'longitude', 'square', 'max_depth', 'description', 'fish', 'boat_downhill',
                    'photos', 'videos')
    column_searchable_list = [Lake.name, ]
    column_default_sort = 'name'
    column_list = ('name', 'latitude', 'longitude', 'square', 'max_depth', 'description', 'fish', 'boat_downhill',
                   'photos', 'videos', 'zec')

class FishView(ModelView):
    column_searchable_list = [Fish.name, ]
    column_default_sort = 'name'
    form_columns = ('name', 'description', 'how_to_catch', 'lakes')
    column_list = ('name', 'description', 'how_to_catch', 'lakes')



@listens_for(Photo, 'after_delete')
def del_image(mapper, connection, target):
    if target.link:
        # Delete image
        try:
            os.remove(os.path.join(photo_path, target.link))
        except OSError:
            pass

        # Delete thumbnail
        try:
            os.remove(os.path.join(photo_path,
                                   form.thumbgen_filename(target.link)))
        except OSError:
            pass


class ImageView(ModelView):
    column_formatters = {'link': format_img}

    # Alternative way to contribute field is to override it completely.
    # In this case, Flask-Admin won't attempt to merge various parameters for the field.
    form_extra_fields = {
        'link': form.ImageUploadField('Image',
                                      base_path=photo_path,
                                      thumbnail_size=(100, 100, True))
    }


# Delete hooks for models, delete files if models are getting deleted
@listens_for(Video, 'after_delete')
def del_file(mapper, connection, target):
    if target.path:
        try:
            os.remove(os.path.join(video_path, target.path))
        except OSError:
            # Don't care if was not deleted because it does not exist
            pass


# Administrative views
class VideoView(ModelView):
    # Override form field to use Flask-Admin FileUploadField
    form_extra_fields = {
        'link': form.FileUploadField('Video',
                                      base_path=video_path)
    }


admin.add_view(UserView(User, db.session))
admin.add_view(ZecView(Zec, db.session))
admin.add_view(LakeView(Lake, db.session))
admin.add_view(FishView(Fish, db.session))
admin.add_view(ImageView(Photo, db.session))
admin.add_view(VideoView(Video, db.session))


